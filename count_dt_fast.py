import pandas as pd
import time

import count_dt_cython

def count(fname='incidents.csv', dt=0.3):
    
    # load data
    data = pd.read_csv(fname).sort_values(['time'], ascending=True)
    # count M dimention
    M = data['feature1'].unique().shape[0]
    # run main function
    count = count_dt_cython.apply_count_in_group( data['id'].values, data['feature1'].values, data['feature2'].values, data['time'].values, 0.3, M)

#     data.sort_values(['id'], inplace=True)
#     data['count'] = count
#     data

    return count


if __name__ == '__main__':
   
    import sys
    fname = sys.argv[1]
    dt = sys.argv[2]

    start_time = time.time()
    count_array = count(fname=fname,dt=dt)
    # count_array = count(fname='incidents__.csv',dt=0.3)
    print("--- %s seconds ---" % (time.time() - start_time))
    print(count_array)


cimport numpy as np
import numpy as np

from cpython cimport array
import array

def init_array(int m):
    cdef int i, j
    cdef object[:, :] A = np.empty((m, m), dtype=object)

    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            A[j, i] = list()

    return A

cdef void _count_in_group(np.ndarray[double] _cnt, object _mstack, float _dt, int _id, int i, int j, float _time):
    cdef float _less = _time -_dt
    while len(_mstack[i][j]) and _mstack[i][j][0] < _less:
        _mstack[i][j].pop(0)
    
    _cnt[_id] = len(_mstack[i][j])
    _mstack[i][j].append( _time )

cpdef np.ndarray[double] apply_count_in_group(np.ndarray _id, np.ndarray _f1, np.ndarray _f2, np.ndarray _time, float dt, int M):
    assert (_id.dtype == np.int and _f1.dtype == np.int and _f1.dtype == np.int and _time.dtype == np.float )
    cdef Py_ssize_t i, n = len(_id)
    assert (len(_f1) == len(_f2) == len(_time) == n)
    cdef np.ndarray[double] res = np.empty(n)
    cdef object _mstack = init_array(M)

    for i in range(n):
        _count_in_group(res, _mstack, dt, _id[i], _f1[i], _f2[i], _time[i])
    return res
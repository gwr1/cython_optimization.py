from distutils.core import setup
from Cython.Build import cythonize

setup(  
    name = 'count dt fast',
    ext_modules = cythonize("count_dt_cython.pyx"),
)

